import './App.css';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import ComposeTeam from './ComposeTeam';
import FirstQuarter from './FirstQuarter';


function App() {
  return (
    <div className="App">
      <Tabs>
        <TabList>
          <Tab>Compose team</Tab>
          <Tab>First Quarter</Tab>
        </TabList>
        <TabPanel>
          <ComposeTeam />
        </TabPanel>
        <TabPanel>
          <FirstQuarter />
        </TabPanel>
      </Tabs>
    </div>
  );
}

export default App;
