import React, { useState } from 'react';
import Multiselect from 'multiselect-react-dropdown';
import { Button } from 'react-bootstrap';
import { playerType } from './utils/constants';
import FormCompoent from './FormCompoent';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';


const schema = yup
  .object()
  .shape({
    fname: yup.string().required(),
    lname: yup.string().required(),
    height: yup.number(),
  });
  


const ComposeTeam = () => {
    const [position, setPosition] = useState([]);
    const [positionError, setPositionError] = useState('')
    const [success, setSuccess] = useState('')

    const {register, handleSubmit, formState: { errors }, reset } = useForm({
        resolver: yupResolver(schema),
    });

    
    const handleSelect=(selectedList, selectValue)=>{
        const playerIds = selectedList.map(player=>player.id);
        setPosition(playerIds);
        setPositionError("");
    }

    const handleRemove = (selectedList) => {
        const playerIds = selectedList.map(player=>player.id) || [];
        setPosition(playerIds);
        if(selectedList.length <= 0)
        setPositionError("Position is required");
    }

    const selectedValue = playerType.filter(player=>{
        return position.includes(player.id);
    });

    const onSubmit = data => {
        let players = localStorage.getItem('players');
        let jsonPlayers = JSON.parse(players) || [];
        if(position.length <= 0){
            setPositionError("Position is required");
            return;
        }
        let id = new Date().getTime();
        const playerData = {...data, ...{position: position, id}};
        jsonPlayers.push(playerData);
        const splayer = JSON.stringify(jsonPlayers);
        localStorage.setItem('players', splayer);
        setSuccess('Player added successfully')
        setPosition([]);
        reset();
    }

    return (
        <div>
            <form method="POST" onSubmit={handleSubmit(onSubmit)} >
                <p className="success">{success}</p>
                <FormCompoent 
                    label="First name" 
                    name="fname" 
                    inputType="text" 
                    register={register}
                    errors={errors}
                />

                <FormCompoent 
                    label="Last name" 
                    name="lname" 
                    inputType="text" 
                    register={register}
                    errors={errors}
                />

                <FormCompoent 
                    label="Height" 
                    name="height" 
                    inputType="number" 
                    min="1"
                    register={register}
                    errors={errors}
                />
                
                <Multiselect 
                    name="position"
                    placeholder="Position"
                    onSelect={handleSelect} 
                    selectedValues={selectedValue} 
                    onRemove={handleRemove}
                    options={playerType} 
                    displayValue="title"
                />
                <p className="error">{positionError}</p>

                <Button variant="primary" type="submit">
                    Save
                </Button>
            </form>
        </div>
    )
}

export default ComposeTeam;
