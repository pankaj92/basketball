import React from 'react';
import { Row, Col, Form, Button } from 'react-bootstrap';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useForm } from 'react-hook-form';
import { playerType } from './utils/constants';


const schema = yup
  .object()
  .shape({
    player_1: yup.string().ensure().when(['player_2','player_3','player_4','player_5'], 
    (player_2,player_3, player_4,player_5, schema, node)=> validatePlayer(player_2,player_3, player_4,player_5, schema, node)),

    player_2: yup.string().ensure().when(['player_1','player_3','player_4','player_5'], 
    (player_1,player_3, player_4,player_5, schema, node)=> validatePlayer(player_1,player_3, player_4,player_5, schema, node)),

    player_3: yup.string().ensure().when(['player_1','player_2','player_4','player_5'], 
    (player_1,player_2, player_4,player_5, schema, node)=> validatePlayer(player_1,player_2, player_4,player_5, schema, node)),

    player_4: yup.string().ensure().when(['player_1','player_2','player_3','player_5'], 
    (player_1,player_2, player_3,player_5, schema, node)=> validatePlayer(player_1,player_2, player_3,player_5, schema, node)),

    player_5: yup.string().ensure().when(['player_1','player_2','player_3','player_4'], 
    (player_1,player_2, player_3,player_4, schema, node)=>validatePlayer(player_1,player_2, player_3,player_4, schema, node)),
    
    position_1: yup.mixed().when('player_1',(player_1, schema)=> validatePosition(player_1,schema)),
    position_2: yup.mixed().when('player_2',(player_2, schema)=> validatePosition(player_2,schema)),
    position_3: yup.mixed().when('player_3',(player_3, schema)=> validatePosition(player_3,schema)),
    position_4: yup.mixed().when('player_4',(player_4, schema)=> validatePosition(player_4,schema)),
    position_5: yup.mixed().when('player_5',(player_5, schema)=> validatePosition(player_5,schema)),
  },[
      ['player_1','player_2'],
      ['player_1','player_3'],
      ['player_1','player_4'],
      ['player_1','player_5'],
      ['player_2','player_3'],
      ['player_2','player_4'],
      ['player_2','player_5'],
      ['player_3','player_4'],
      ['player_3','player_5'],
      ['player_4','player_5'],
    ]);

  const validatePlayer = (player_1, player_2, player_3, player_4, schema,node) => {
    let players = [player_1,player_2, player_3,player_4];
    return node.value == '' ? schema.required() : schema.notOneOf(players,'Player can be select only once');
  }

  const validatePosition = (playerId, schema) =>{
    const playerstring = localStorage.getItem('players');
        const playerOption = JSON.parse(playerstring) || [];
        let playerType = playerOption.filter((player)=>{
            // console.log(`${player.id} === ${player}`)
            return player.id == playerId;
        });
        return playerId ? schema.oneOf(playerType[0]?.position) : schema.required();
}

function FirstQuarter() {
    const playerstring = localStorage.getItem('players');
    const playerOption = JSON.parse(playerstring) || [];
    const ranges = [1,2,3,4,5];

    const {register, handleSubmit, formState: { errors } } = useForm({
        resolver: yupResolver(schema),
    });

    console.error('errors', errors)

    const onSubmit = data => {
        console.log(data);
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
        <div>
            {ranges.map((range)=>{
                let playerName = `player_${range}`;
                let positionName = `position_${range}`;
                return (
                    <Form.Group className="mb-3" key={range}>
                        <Row >
                            <Col>
                                <Form.Select name={playerName} {...register(playerName)}>
                                    <option value=''>Select player</option>
                                    { playerOption.map((player,key)=>{
                                        return <option key={key} value={player.id}>{player.fname}</option>
                                    })}
                                </Form.Select>
                                <p className="error">{errors[playerName]?.message}</p>
                            </Col>
                            <Col>
                                <Form.Select name={positionName} {...register(positionName)}>
                                    <option value=''>Select position</option>
                                    { playerType.map((type,keys)=>{
                                    return <option key={keys} value={type.id}>{type.title}</option>
                                    })}
                                </Form.Select>
                                <p className="error">{errors[positionName]?.message}</p>
                            </Col>
                        </Row>
                    </Form.Group>
                );
            })}
            <Button variant="primary" type="submit">
                Save
            </Button>
        </div>
        </form>
    )
}

export default FirstQuarter;
