import React from 'react';
import { FloatingLabel, Form } from 'react-bootstrap';

const FormCompoent = (props) => {

    const { label, name, inputType, min, register,errors} = props;
    return (
        <>
          <FloatingLabel
                controlId="floatingInput"
                label={label}
                className="mb-3"
            >
                <Form.Control 
                    name={name}
                    type={inputType} 
                    min={(inputType === 'number') ? min : ''}
                    {...register(name)}
                />
                <p className="error">{errors[name]?.message}</p>
            </FloatingLabel>  
        </>
    )
}

export default FormCompoent
