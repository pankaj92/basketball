export const playerType = [
        {
            id: 'PG',
            title: 'Point guard (PG)'
        },
        {
            id: 'SG',
            title: 'Shooting guard (SG)'
        },
        {
            id: 'SF',
            title: 'Small forward (SF)'
        },
        {
            id: 'PF',
            title: 'Power forward (PF)'
        },
        {
            id: 'C',
            title: 'Center (C)'
        }   
    ];